import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   @Override
   public String toString() {
      if (firstChild == null)
          return name;
      StringBuffer b = new StringBuffer();
      b.append(name)
              .append("(")
              .append(firstChild)
              .append(",")
              .append(firstChild.nextSibling)
              .append(")");
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Stack<Tnode> stack = new Stack<>();
      List<String> splitRPN = splitBySpace(pol);
      try {
         for (String s : splitRPN) {
            Tnode n = new Tnode();
            n.name = s;
            if (!isStringParsable(s)) {
               Tnode siblingOfChild = stack.pop();
               n.firstChild = stack.pop();
               n.firstChild.nextSibling = siblingOfChild;
            }
            stack.push(n);
         }
         if (stack.size() != 1)
            throw new RuntimeException("Given RPM has surplus of elements.");
         return stack.pop();
      } catch (EmptyStackException e) {
         throw new RuntimeException("Given RPM is invalid.");
      }
   }
   private static List<String> splitBySpace(String s) {
      List<String> strings = new ArrayList<>();
      int start = 0;
      int end = s.indexOf(" ", start);
      if (end == 0)
         strings.add(s);
      while (end > 0) {
         try {
            String sub = s.substring(start, end).trim();
            if (!sub.isEmpty())
               strings.add(sub);
            start = end + 1;
         } catch (IndexOutOfBoundsException ignored) {
         }
         end = s.indexOf(" ", start);
      }
      String sub = s.substring(start).trim();
      if (!sub.isEmpty())
         strings.add(sub);
      return strings;
   }

   public static boolean isStringParsable(String s) {
      try {
         Integer.parseInt(s);
         return true;
      } catch (final NumberFormatException e) {
         return false;
      }
   }

   public static void main (String[] param) {
      String rpn = "512 1 - 4 * -61 3 / +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res.toString());
   }
}

